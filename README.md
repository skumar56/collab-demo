Django demo setup guide - 
	
	Download the code from the gitlab repository
$ git clone https://gitlab-beta.engr.illinois.edu/skumar56/collab-demo.git


Once, you have downloaded the repository, ensure that you have downloaded the necessary packages (requirements)

python >= 2.7.10

django >= 1.10.2

Whoosh >= 2.7.4

Bootstrap > 4.0.0


Alternatively, you can use the virtual environment by adopting the following commands, 


$ pip install virtualenv

$ virtualenv venv

$ source venv/bin/activate

$ cd collab-demo/

$ cd author_search/

$ pip install -r requirements.txt


The above code snippet should download your necessary requirements file in the virtual environment. 


Thereafter we simply run the Django code using

	$ python manage.py runserver


This starts a Django local server in your system which can be accessed by default at 127.0.0.1:8000/	