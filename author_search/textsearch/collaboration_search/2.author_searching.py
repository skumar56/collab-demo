import timeit
from whoosh import fields, index, scoring, qparser
from whoosh.index import *
from whoosh.fields import *
from whoosh.qparser import *
from whoosh.scoring import *
from heapq import nlargest
from operator import itemgetter

def calc_ensemble_scores(input_name):
	# weighted average scores 
	files = ['tfidf_PAPA_author_list', 'tfidf_PA_author_list', 'bm25_PVPA_author_list', 'bm25_PAPA_author_list', 'bm25_PA_author_list', 'tfidf_PVPA_author_list']
	auth_dict = {}
	for fl in files:
		f_paper = open('./textsearch/collaboration_search/' + input_name + '/' + fl + '.txt')
		first = True

		for line in f_paper:
			if first:
				mx = float(line.split('\t')[1])
				first = False
			if line.split('\t')[0] not in auth_dict:
				auth_dict[line.split('\t')[0]] = 0
			auth_dict[line.split('\t')[0]] += float(line.split('\t')[1]) / mx 
			
		f_paper.close()

	auth_score = open( './textsearch/collaboration_search/' + input_name + "/" +'ensemble_author_list.txt', 'w')
	for auth, score in nlargest(20, auth_dict.iteritems(), key=itemgetter(1)):
		auth_score.write(str(auth) + "\t" + str(score) + "\n")


def calc_paper_scores(input_name):
	#create dictionary for author_name
	paper_dict = {}
	with open("./textsearch/collaboration_search/paper_data/paper_title.txt") as f:
		for l in f:
			l = l.strip().split("\t")
			paper_dict[l[0]] = l[1]

	pap_score = open( './textsearch/collaboration_search/' + input_name +'/paper_list.txt', 'w')
	
	score = 0
	for pid, ptitle in paper_dict.iteritems():
		pap_score.write(ptitle + "\t" + str(score) + "\n")
		score +=1
		if score == 20:
			break

def calc_auth_scores(score_d, filename, algo, input_name):
	#create dictionary for author_name
	auth_dict = {}
	with open("./textsearch/collaboration_search/paper_data/author.txt") as f:
		for l in f:
			l = l.strip().split("\t")
			auth_dict[l[0]] = l[1]

	auth_ws = {}
	auth_w = {}
	auth_score = open( './textsearch/collaboration_search/' + input_name + "/" + algo+ "_"+filename+'_author_list.txt', 'w')
	
	with open("./textsearch/collaboration_search/paper_data/"+filename+".txt") as f:
		for line in f:
			l = line.strip().split("\t")
			paper = l[0]
			author = l[1]
			wght = float(l[2])
			if paper in score_d:
				if author not in auth_ws:
					auth_ws[author] = wght*score_d[paper]
					auth_w[author] = wght
				else:
					auth_ws[author] += wght*score_d[paper]
					auth_w[author] += wght
	for auth in auth_ws:
		auth_ws[auth] = auth_ws[auth]/auth_w[auth]
	for auth, score in nlargest(20, auth_ws.iteritems(), key=itemgetter(1)):
		auth_score.write(str(auth_dict[auth]) + "\t" + str(score) + "\n")


def bm25(query, input_name, output_name):
	# '***** BM 25 **** '
	
	schema = fields.Schema(paper_id=fields.NUMERIC(stored=True, sortable=True), paper_txt=fields.TEXT(stored=True))
	ix = open_dir("./textsearch/collaboration_search/paper_index/title_index")
	score_d={}
	parser = qparser.QueryParser("paper_txt", schema=schema, group=qparser.OrGroup)
	paper_score = open('./textsearch/collaboration_search/' + input_name +'/bm25_paper_list.txt', 'w')
	pid_title = {}
	with open("./textsearch/collaboration_search/paper_data/paper_title.txt") as f:
		for line in f:
			l=line.strip().split("\t")
			pid_title[l[0]] = l[1]
	with ix.searcher() as searcher:
		
		# query = QueryParser("paper_txt", ix.schema).parse(u'kernel density function is a funny thing hahah')
		query = parser.parse(unicode(query))
		results = searcher.search(query, limit = 728455)
		
		for i, hit in enumerate(results):
			score_d[hit['paper_id']] = results.score(i)

		for pid, score in nlargest(20, score_d.iteritems(), key=itemgetter(1)):
			paper_score.write(str(pid_title[pid]) + "\t" + str(score) + "\n")	

	if 'PA' in output_name or 'ensemble' in output_name:
		calc_auth_scores(score_d, "PA", "bm25", input_name)
	if 'PAPA' in output_name or 'ensemble' in output_name:
		calc_auth_scores(score_d, "PAPA", "bm25", input_name)
	if 'PVPA' in output_name or 'ensemble' in output_name:
		calc_auth_scores(score_d, "PVPA", "bm25", input_name)


def tfidf(query, input_name, output_name):
	#'***** TF IDF **** '
	schema = fields.Schema(paper_id=fields.NUMERIC(stored=True, sortable=True), paper_txt=fields.TEXT(stored=True))
	ix = open_dir("./textsearch/collaboration_search/paper_index/title_index")
	score_tf_d={}
	parser = qparser.QueryParser("paper_txt", schema=schema, group=qparser.OrGroup)
	paper_score = open('./textsearch/collaboration_search/' + input_name +'/tfidf_paper_list.txt', 'w')
	pid_title = {}
	with open("./textsearch/collaboration_search/paper_data/paper_title.txt") as f:
		for line in f:
			l=line.strip().split("\t")
			pid_title[l[0]] = l[1]
	with ix.searcher(weighting=scoring.TF_IDF()) as searcher:
		# query = QueryParser("paper_txt", ix.schema).parse(u'kernel density function is a funny thing hahah')
		query = parser.parse(unicode(query))
		results = searcher.search(query, limit = 728455)
		for i, hit in enumerate(results):
			score_tf_d[hit['paper_id']] = results.score(i)

		for pid, score in nlargest(20, score_tf_d.iteritems(), key=itemgetter(1)):
			paper_score.write(str(pid_title[pid]) + "\t" + str(score) + "\n")

	if 'PA' in output_name or 'ensemble' in output_name:
		calc_auth_scores(score_tf_d, "PA", "tfidf", input_name)
	if 'PAPA' in output_name or 'ensemble' in output_name:
		calc_auth_scores(score_tf_d, "PAPA", "tfidf", input_name)
	if 'PVPA' in output_name or 'ensemble' in output_name:
		calc_auth_scores(score_tf_d, "PVPA", "tfidf", input_name)	


def main():
	input_name = sys.argv[1]
	output_name = sys.argv[2]
	# print output_name
	os.system('rm -r ./textsearch/collaboration_search/' + input_name)
	os.system('mkdir ./textsearch/collaboration_search/' + input_name)
	input_f = open('./textsearch/collaboration_search/' + input_name + ".txt")
	query=input_f.read()
	if 'bm25' in output_name or 'ensemble' in output_name:
		bm25(query, input_name, output_name)
	if 'tfidf' in output_name or 'ensemble' in output_name:
		tfidf(query, input_name, output_name)
	# calc_paper_scores(input_name)
	if 'ensemble' in output_name:
		calc_ensemble_scores(input_name)

	

if __name__ == '__main__':
	main()