from django.shortcuts import render
from models import Queries
# from _packages import *
import os

def home(request):
	return render(request, 'textsearch/home.html')

def look(request):
	print 'here'
	return render(request, 'textsearch/look.html')


def post_detail(request):
	# print 'posting detail'
	return render(request, 'textsearch/basic.html', {'content' : ['Contact us at <b> atreya2@illinois.edu, skumar56@illinois.edu</b> for further assistance, information, collaboration, etc.']})


def display(request):
    questions=None
    if request.GET.get('name'):
        # search = request.GET.get('search')
        # questions = Queries.objects.filter(query__icontains=search)
        name = request.GET.get('name')
        print name, request.GET.get('querytp'), request.GET.get('pathsim')


        qout = open('./textsearch/collaboration_search/query.txt', 'w')
        qout.write(name)
        qout.close()



        output_name = ''
        output_name += request.GET.get('querytp') + '_'
        output_name += request.GET.get('pathsim') + '_'
        output_name += request.GET.get('standalone') + '_'


        os.system('pwd')
        os.system('python ./textsearch/collaboration_search/2.author_searching.py query '+output_name)


        if request.GET.get('standalone') == 'standalone':
            auths = []
            f_paper = open('./textsearch/collaboration_search/query/'+request.GET.get('querytp') + '_'  + request.GET.get('pathsim') + '_author_list.txt')
            for line in f_paper:
                auths.append(line.strip().split('\t')[0])
            f_paper.close()
            questions = {'Search result for ' + request.GET.get('querytp') + ' and ' + request.GET.get('pathsim') : auths}
        else:
            auths = []
            f_paper = open('./textsearch/collaboration_search/query/'+'ensemble_author_list.txt')
            for line in f_paper:
                auths.append(line.strip().split('\t')[0])
            f_paper.close()
            questions = {'Search result for ' + 'ensemble' : auths}




        # questions = {'first' : ['1', '2', '3'], 'second' : ['a', 'b', 'c'], 'third' : ['z', 'x', 'y'], 'fourth' :['l', 'll', 'llll']}
        # questions = [name, 'kalia']
        # query = Queries.object.create(query=search, user_id=name)
        # query.save()

    return render(request, 'textsearch/display_new.html',{'questions': questions, })
