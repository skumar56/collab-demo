import os 
import sys
import timeit
import random
import time
import shutil
import bisect
import warnings
import logging
warnings.filterwarnings("ignore")

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib.mlab
import seaborn as sns; sns.set(color_codes=True)

import numpy as np
import numpy.polynomial.polynomial as poly
from scipy import stats
from scipy.optimize import curve_fit
from scipy.stats import chisquare
from scipy.stats import norm
from sklearn.preprocessing import normalize
from sklearn.cluster import KMeans
from sklearn.cluster import KMeans
from sklearn import metrics
from sklearn.metrics import pairwise_distances
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.svm import SVC
from sklearn.utils import shuffle

from igraph import *

import math
import collections
from collections import Counter
import pandas as pd
from datetime import datetime
import pylab as pl
import copy
import colorsys
import emcee
from cycler import cycler
import multiprocessing as mp
from multiprocessing import Pool, TimeoutError
import xml.etree.ElementTree as ET
from xml.dom import minidom
from xml.etree import ElementTree
from lxml import etree
import scipy
from scipy.sparse import *
from scipy import sparse
import itertools
import collections
